﻿using UnityEngine;
// using System.Collections;
using System.Collections.Generic;

public class GameStateManager : MonoBehaviour {

    public GameObject fieldPrefab;
    public float initialFallTime;
    public float horizontalFirstDelay;
    public float horizontalContinuousDelay;
    public float downFirstDelay;
    public float downContinuousDelay;

    private List<FieldCache> fields = new List<FieldCache>();
    private int selectedFieldIndex;
    private List<TetrominoCache> activeTetrominoes = new List<TetrominoCache>();
    private ActiveTetrominoState activeTetrominoState;

	// Use this for initialization
	void Start () {
        // initialize fall time.
        // initialFallTime = 0.8F;

        // Add field instances into the field list.
        AddNewField(new Vector2(-15, 0), true);
        AddNewField(new Vector2(0, 0), false);
        AddNewField(new Vector2(15, 0), false);


        // Initialize the current tetromino state.
        activeTetrominoState = new ActiveTetrominoState(horizontalFirstDelay, horizontalContinuousDelay, downFirstDelay, downContinuousDelay);

        // Initialize the selected field to the first one.
        selectedFieldIndex = 0;
        // fields[0].fieldBorder.SetActive();
        // HandleFieldChange(0);

        // Start the spawner of all fields.
        StartAllSpawners();
    }

    // Adds a new field onto the playing canvas.
    // Also adds a new 'slot' into activeTetrominoes.
    void AddNewField(Vector2 coordinates, bool setActive) {
        var newField = new FieldCache((GameObject)Instantiate(fieldPrefab, new Vector3(coordinates.x, coordinates.y, 0), Quaternion.identity));
        newField.spawner.SetFallTime(initialFallTime);
        if (setActive) {
            newField.fieldBorder.SetActive();
        }
        else {
            newField.fieldBorder.SetInactive();
        }
        fields.Add(newField);

        activeTetrominoes.Add(null);
    }

    // Update is called once per frame
    void Update () {
        // Change the selected field according to user input.
        if (Input.GetKeyDown(KeyCode.Alpha1)) {
            HandleFieldChange(0);
        }

        if (Input.GetKeyDown(KeyCode.Alpha2)) {
            HandleFieldChange(1);
        }

        if (Input.GetKeyDown(KeyCode.Alpha3)) {
            HandleFieldChange(2);
        }

        if (Input.GetKeyDown(KeyCode.Alpha4)) {
            HandleFieldChange(3);
        }

        if (Input.GetKeyDown(KeyCode.Alpha5)) {
            HandleFieldChange(4);
        }

        if (Input.GetKeyDown(KeyCode.Alpha6)) {
            HandleFieldChange(5);
        }

        // Move the active tetromino of the selected field according to user input,
        // and handle key down state change.
        if (Input.GetKeyDown(KeyCode.LeftArrow)) {
            MoveActiveTetrominoLeft(selectedFieldIndex);
            activeTetrominoState.HandleKeyDown(ActiveTetrominoState.Direction.Left);
        }

        if (Input.GetKeyDown(KeyCode.RightArrow)) {
            MoveActiveTetrominoRight(selectedFieldIndex);
            activeTetrominoState.HandleKeyDown(ActiveTetrominoState.Direction.Right);
        }

        if (Input.GetKeyDown(KeyCode.UpArrow)) {
            RotateActiveTetromino(selectedFieldIndex);
        }

        if (Input.GetKeyDown(KeyCode.DownArrow)) {
            MoveActiveTetrominoDown(selectedFieldIndex);
            activeTetrominoState.HandleKeyDown(ActiveTetrominoState.Direction.Down);
        }

        if (Input.GetKeyDown(KeyCode.Space)) {
            DropActiveTetromino(selectedFieldIndex);
        }

        // Handle key up state change.
        if (Input.GetKeyUp(KeyCode.LeftArrow)) {
            activeTetrominoState.HandleKeyUp(ActiveTetrominoState.Direction.Left);
        }

        if (Input.GetKeyUp(KeyCode.RightArrow)) {
            activeTetrominoState.HandleKeyUp(ActiveTetrominoState.Direction.Right);
        }

        if (Input.GetKeyUp(KeyCode.DownArrow)) {
            activeTetrominoState.HandleKeyUp(ActiveTetrominoState.Direction.Down);
        }

        // Move the active tetromino of the current field according to it's state.
        if (activeTetrominoState.CanMoveTo(ActiveTetrominoState.Direction.Left)) {
            MoveActiveTetrominoLeft(selectedFieldIndex);
            activeTetrominoState.UpdateMovementState(ActiveTetrominoState.Direction.Left);
        }

        if (activeTetrominoState.CanMoveTo(ActiveTetrominoState.Direction.Right)) {
            MoveActiveTetrominoRight(selectedFieldIndex);
            activeTetrominoState.UpdateMovementState(ActiveTetrominoState.Direction.Right);
        }

        if (activeTetrominoState.CanMoveTo(ActiveTetrominoState.Direction.Down)) {
            MoveActiveTetrominoDown(selectedFieldIndex);
            activeTetrominoState.UpdateMovementState(ActiveTetrominoState.Direction.Down);
        }

        // Move the active tetrominoes according to timed falling.
        for (int i = 0 ; i < activeTetrominoes.Count ; i++) {
            var currentTetromino = activeTetrominoes[i].tetromino;
            if (currentTetromino.HasToFall()) {
                MoveActiveTetrominoDown(i);
            }
        }
	}

    void MoveActiveTetrominoLeft (int fieldIndex) {
        fields[fieldIndex].field.MoveTetrominoLeft(activeTetrominoes[fieldIndex].tetromino.transform);
    }

    void MoveActiveTetrominoRight (int fieldIndex) {
        fields[fieldIndex].field.MoveTetrominoRight(activeTetrominoes[fieldIndex].tetromino.transform);   
    }

    void RotateActiveTetromino (int fieldIndex) {
       fields[fieldIndex].field.RotateTetromino(activeTetrominoes[fieldIndex].tetromino.transform);
    }

    void MoveActiveTetrominoDown (int fieldIndex) {
        activeTetrominoes[fieldIndex].tetromino.SetLastFall(Time.time);
        bool tetrominoFixed = fields[fieldIndex].field.MoveTetrominoDown(activeTetrominoes[fieldIndex].tetromino.transform);

        // if the current tetromino cannot move further,
        // spawn the next tetromino for this field.
        if (tetrominoFixed) {
            StartSpawner(fieldIndex);
        }
    }

    void DropActiveTetromino (int fieldIndex) {
        fields[fieldIndex].field.DropTetromino(activeTetrominoes[fieldIndex].tetromino.transform);

        // This tetromino cannot move further.
        StartSpawner(fieldIndex);
    }

    // Spawns a new tetromino for every field.
    void StartAllSpawners () {
        for (int i = 0 ; i < fields.Count ; i++) {
            StartSpawner(i);
        }
    }

    // Spawns a new tetromino for the i'th field.
    void StartSpawner (int i) {
        activeTetrominoes[i] = new TetrominoCache(fields[i].spawner.SpawnNext());
    }

    // Handles field change.
    void HandleFieldChange (int newFieldIndex) {
        ChangeSelectedField(newFieldIndex);

        // Reset the tetromino state,
        // because we don't want inputs for one field influence another.
        activeTetrominoState.Reset(); 
    }

    // Changes the field of which the player has control of.
    void ChangeSelectedField (int newFieldIndex) {
        if (IsValidFieldIndex(newFieldIndex)) {
            // If the new field is a different one,
            // switch the appropriate field border sprites.
            if (newFieldIndex != selectedFieldIndex) {
                fields[newFieldIndex].fieldBorder.SetActive();
                fields[selectedFieldIndex].fieldBorder.SetInactive();
            }

            selectedFieldIndex = newFieldIndex;
        }
    }

    // Returns whether the given field index is valid.
    bool IsValidFieldIndex (int fieldIndex) {
        return (fieldIndex < fields.Count);
    }
}
