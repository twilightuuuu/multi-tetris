﻿using UnityEngine;
using System.Collections;

public class FieldCache {

	public Field field;
	public FieldBorder fieldBorder;
	public TetrominoSpawner spawner;

	public FieldCache(GameObject f) {
		field = f.GetComponent<Field>();
		fieldBorder = f.GetComponentInChildren<FieldBorder>();
		spawner = f.GetComponentInChildren<TetrominoSpawner>();
	}

	// Use this for initialization
	// void Start () {
	
	// }
	
	// Update is called once per frame
	// void Update () {
	
	// }
}
