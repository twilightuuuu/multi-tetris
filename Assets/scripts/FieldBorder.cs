﻿using UnityEngine;
using System.Collections;

public class FieldBorder : MonoBehaviour {

	public Sprite activeSprite;
	public Sprite inactiveSprite;

	// Use this for initialization
	void Start () {
		// SetInactive();
	}
	
	// // Update is called once per frame
	// void Update () {
	
	// }

	public void SetActive () {
		transform.GetComponent<SpriteRenderer>().sprite = activeSprite;
	}

	public void SetInactive () {
		transform.GetComponent<SpriteRenderer>().sprite = inactiveSprite;
	}
}
