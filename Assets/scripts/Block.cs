﻿using UnityEngine;
using System.Collections;

public class Block : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public IEnumerator Blink (float duration, int numBlinks) {
		var material = transform.GetComponent<SpriteRenderer>().material;
		float elapsedTime = 0f;
		float flashAmount = 0f;
		while (elapsedTime <= duration) {
			flashAmount = Mathf.PingPong(elapsedTime * (numBlinks * 2) / duration, 1f);
			material.SetFloat("_FlashAmount", flashAmount);
			elapsedTime += Time.deltaTime;
			yield return null;
		}

		material.SetFloat("_FlashAmount", 0f);

		// var material = transform.GetComponent<SpriteRenderer>().material.SetFloat("_FlashAmount", float value)
	}
}
