﻿using UnityEngine;
using System.Collections;

public class Tetromino : MonoBehaviour {

    public Vector3 spawnOffset;
    private float fallTime;
    private float lastFall = 0;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SetFallTime(float t) {
		fallTime = t;
	}

	public void SetLastFall(float time) {
		lastFall = time;
	}

	public bool HasToFall() {
		return (Time.time - lastFall >= fallTime);
	}

	public void Blink() {
		foreach (var block in transform.GetComponentsInChildren<Block>()) {
			StartCoroutine(block.Blink(0.3f, 1));
			// block.Blink(1f);
		}
	}
}
