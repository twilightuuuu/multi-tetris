﻿using UnityEngine;
using System.Collections;

public class Field : MonoBehaviour {

	public static int width = 10;
	public static int height = 22;
	public Transform[,] grid = new Transform[width,height];

	private Vector2 gridPosition;

	// Use this for initialization
	void Start () {
		// we set gridPosition to the coordinates a block on the left-most bottom square would be on.
		// we add 0.5F to both axes because the block takes up 1*1 units.
		gridPosition = new Vector2(transform.position.x - width/2 + 0.5F, transform.position.y - height/2 + 0.5F);
	}
	
	// Update is called once per frame
	// void Update () {
	
	// }

    public void MoveTetrominoLeft(Transform tetromino) {
        MoveTetrominoSideways(tetromino, false);
    }

    public void MoveTetrominoRight(Transform tetromino) {
        MoveTetrominoSideways(tetromino, true);
    }

    // Moves the given tetromino sideways.
    // Updates visuals & the grid.
    public void MoveTetrominoSideways(Transform tetromino, bool is_right) {
        int direction = is_right ? 1 : -1;

        // Try to move the tetromino to the given direction.
        tetromino.position += new Vector3(direction, 0, 0);

        // Is the new position valid?
        if(IsValidGridPos(tetromino)) {
            UpdateGrid(tetromino);
        }
        else {
            // Revert to the old position.
            tetromino.position += new Vector3(direction * -1, 0, 0);
        }
    }

    // Rotates the given tetromino.
    // Updates visuals & the grid.
    public void RotateTetromino(Transform tetromino) {
        tetromino.Rotate(0, 0, -90);

        // See if valid
        if (IsValidGridPos(tetromino))
            // It's valid. Update grid.
            UpdateGrid(tetromino);
        else
        {
            // Wall-kick #1: Try moving it once to the right.
            tetromino.position += new Vector3(1, 0, 0);
            if (IsValidGridPos(tetromino))
                UpdateGrid(tetromino);
            else
            {
                // Wall-kick #2: Try moving it once to the left.
                // (We actually move to twice to the left in order to undo the previous movement.
                tetromino.position += new Vector3(-2, 0, 0);
                if (IsValidGridPos(tetromino))
                    UpdateGrid(tetromino);
                else
                {
                    // It's not valid. revert.
                    tetromino.position += new Vector3(1, 0, 0);
                    tetromino.Rotate(0, 0, 90);
                }
            }

            
        }
    }

    // Moves the given tetromino down.
    // Updates visuals & the grid.
    // Returns whether the given block cannot move anymore.
    public bool MoveTetrominoDown(Transform tetromino) {
        // Modify position
            tetromino.position += new Vector3(0, -1, 0);

            // See if valid
            if (IsValidGridPos(tetromino)) {
                // It's valid. Update grid.
                UpdateGrid(tetromino);
                return false;
            }
            else {
                // It's not valid. revert.
                tetromino.position += new Vector3(0, 1, 0);

                tetromino.GetComponentInParent<Tetromino>().Blink();

                // Clear filled horizontal lines
                DeleteFullRows();

                return true;
            }
    }

    // Drops the given tetromino as far down as possible.
    // Updates visuals & the grid.
    public void DropTetromino(Transform tetromino) {
        while (IsValidGridPos(tetromino)) {
            tetromino.position += new Vector3(0, -1, 0);
        }

        // Since the resulting position is not valid,
        // we pull the tetromino one space up,
        // update the grid and delete any full rows.
        tetromino.position += new Vector3(0, 1, 0);
        tetromino.GetComponentInParent<Tetromino>().Blink();
        UpdateGrid(tetromino);
        DeleteFullRows();
    }

	public Vector2 OffsetWithinGrid(Vector2 v)
    {
        return new Vector2(Mathf.Round(v.x - gridPosition.x), Mathf.Round(v.y - gridPosition.y));
    }

    public bool InsideBorder(Vector2 pos)
    {
        return ((int)pos.x >= 0 && (int)pos.x < width && (int)pos.y >= 0);
    }

    public void DeleteRow(int y)
    {
        for (int x = 0; x < width; x++)
        {
            Destroy(grid[x, y].gameObject);
            grid[x, y] = null;
        }
    }

    public void DecreaseRow(int y)
    {
        for(int x = 0; x < width; x++)
        {
            if(grid[x, y] != null)
            {
                grid[x, y - 1] = grid[x, y];
                grid[x, y] = null;

                grid[x, y - 1].position += new Vector3(0, -1, 0);
            }
        }
    }

    public void DecreaseRowsAbove(int y)
    {
        for (int i = y; i < height; i++)
        {
            DecreaseRow(i);
        }
    }

    public bool IsRowFull(int y)
    {
        for (int x = 0; x < width; x++)
        {
            if (grid[x, y] == null)
            {
                return false;
            }
        }

        return true;
    }

    public void DeleteFullRows()
    {
        for (int y = 0; y < height; y++)
        {
            if (IsRowFull(y))
            {
                DeleteRow(y);
                DecreaseRowsAbove(y + 1);
                y--;
            }
        }
    }

    bool IsValidGridPos(Transform tetromino)
    {
        foreach (Transform child in tetromino)
        {
            Vector2 v = OffsetWithinGrid(child.position);

            // Not inside Border?
            if (!InsideBorder(v))
                return false;

            // Block in grid cell (and not part of same group)?
            if (grid[(int)v.x, (int)v.y] != null && grid[(int)v.x, (int)v.y].parent != tetromino)
                return false;
        }
        return true;
    }


    public void UpdateGrid(Transform newTransform) {
    	// Remove old children from grid
    	for (int y = 0; y < Field.height; ++y)
    	    for (int x = 0; x < Field.width; ++x)
    	        if (grid[x, y] != null)
    	            if (grid[x, y].parent == newTransform)
    	                grid[x, y] = null;

    	// Add new children to grid
    	foreach (Transform child in newTransform)
    	{
    	    Vector2 v = OffsetWithinGrid(child.position);
    	    grid[(int)v.x, (int)v.y] = child;
    	}
    }
}
