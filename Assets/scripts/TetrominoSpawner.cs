﻿using UnityEngine;
using System.Collections;

public class TetrominoSpawner : MonoBehaviour {

    public GameObject[] tetrominoes;
    private float fallTime;

	// Use this for initialization
	void Start () {
        // SpawnNext();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SetFallTime(float t) {
		fallTime = t;
	}

    // Spawn and return a new tetromino.
    public GameObject SpawnNext()
    {
        int i = Random.Range(0, tetrominoes.Length);
        var t = (GameObject)Instantiate(tetrominoes[i], transform.position + tetrominoes[i].GetComponent<Tetromino>().spawnOffset, Quaternion.identity);
        t.GetComponent<Tetromino>().SetFallTime(fallTime);

        return t;
    }
}
