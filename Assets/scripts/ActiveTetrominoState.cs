﻿using UnityEngine;
using System.Collections;

public class ActiveTetrominoState {
	
	public enum Direction {
		None,
		Left,
		Right,
		Down
	};

	public enum MovementStatus {
		None,
		First,
		Continuous
	};

	private Direction horizontalDirection;
	private MovementStatus horizontalMovementStatus;
	private float horizontalLastMove = 0;
	private float horizontalFirstMoveDelay;
	private float horizontalContinuousMoveDelay;

	private MovementStatus downMovementStatus;
	private float downLastMove = 0;
	private float downFirstMoveDelay;
	private float downContinuousMoveDelay;

	public ActiveTetrominoState (float horFirstDelay, float horContDelay, float downFirstDelay, float downContDelay) {
		horizontalDirection = Direction.None;
		horizontalMovementStatus = MovementStatus.None;
		downMovementStatus = MovementStatus.None;

		horizontalFirstMoveDelay = horFirstDelay;
		horizontalContinuousMoveDelay = horContDelay;
		downFirstMoveDelay = downFirstDelay;
		downContinuousMoveDelay = downContDelay;
	}

	// Handles state logic for a key down action.
	public void HandleKeyDown (Direction d) {
		if (d == Direction.Left || d == Direction.Right) {
			horizontalLastMove = Time.time;
			horizontalMovementStatus = MovementStatus.First;
			horizontalDirection = d;
		}
		else if (d == Direction.Down) {
			downLastMove = Time.time;
			downMovementStatus = MovementStatus.First;
		}
	}

	// Handles state logic for a key up action.
	public void HandleKeyUp (Direction d) {
		if (d == Direction.Left || d == Direction.Right) {
			// If the released key is the same as the current direction,
			// reset movement.
			// Else, do nothing. (releasing left while right is pressed should do nothing)
			if (d == horizontalDirection) {
				horizontalMovementStatus = MovementStatus.None;
				horizontalDirection = Direction.None;
			}
		}
		else if (d == Direction.Down) {
			downMovementStatus = MovementStatus.None;
		}
	}

	// Returns whether this tetromino can move to the given direction,
	// due to continuous key down input.
	public bool CanMoveTo (Direction d) {
		if (d == Direction.Left || d == Direction.Right) {
			if (horizontalDirection == d) {
				if ((horizontalMovementStatus == MovementStatus.First && Time.time - horizontalLastMove > horizontalFirstMoveDelay) || (horizontalMovementStatus == MovementStatus.Continuous && Time.time - horizontalLastMove > horizontalContinuousMoveDelay)) {
					return true;
				}
			}
		}
		else if (d == Direction.Down) {
			if ((downMovementStatus == MovementStatus.First && Time.time - downLastMove > downFirstMoveDelay) || (downMovementStatus == MovementStatus.Continuous && Time.time - downLastMove > downContinuousMoveDelay)) {
				return true;
			}
		}

		return false;
	}

	// Updates the movement state after this tetromino moved in the given direction.
	public void UpdateMovementState (Direction d) {
		if (d == Direction.Left || d == Direction.Right) {
			horizontalLastMove = Time.time;
			horizontalMovementStatus = MovementStatus.Continuous;
		}
		else if (d == Direction.Down) {
			downLastMove = Time.time;
			downMovementStatus = MovementStatus.Continuous;
		}
	}

	// Resets the state.
	public void Reset () {
		horizontalMovementStatus = MovementStatus.None;
		horizontalDirection = Direction.None;
		downMovementStatus = MovementStatus.None;
	}
}
